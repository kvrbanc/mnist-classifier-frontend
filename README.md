# MNIST-classifier-frontend

This repository contains code that implements the frontend part of the MNIST-classifier system. The system consists of Frontend, Backend, and Classifier service. Git repositories of the remaining parts of the system:
- Backend: https://gitlab.com/kvrbanc/mnist-classifier-backend
- Classifier service: https://gitlab.com/kvrbanc/mnist-classifier

## Usage of the frontend

The *main page* of the frontend is accessible through the URL: http://localhost:4200/

If the user is not already logged in, he will be redirected to the **login page**. User can log in using *email* and *password*. 

If the user doen't have an account alrady, he can create one by accessing the **register page**. There he can register a new account by providing an *email*, *password* and *retyping* the password. After registration, an email is sent to the user's email address, contining *verification link*. The user has to verify his account in order to access the application. The account is verified by clicking on the *verification link*, which opens up the **account verification** page, with a form already filled up with user's *verification code*. The user need to click the *verify* button to verify his account.

If the user has an account, but has forgot his password, he can reset his password by visting the **forgot password page**. There he can input his email to which a *password reset link* will be sent. To reset his password, user needs to click on the *password reset link*, which will open up a **password reset page**. There he cen set up a new password, by inputing the new *password* and *retyping the password*.

(**Note**: At this stage, the frontend application *does not provide* any warning/error/success messages to the user, so the user needs to be careful when inputting the requested information)

(**Note**: The emails provided *must be in valid form*, and the passwords *must be at least 8 characters long*)

After the user has logged in, he will be redirected to the **main page** of the application. There he will see 4 graphs showing 4 digits from the MNIST dataset. Beneath the graphs, *Predict* buttons are located. These buttons are used to *predict* the label presented on the graph using the Classifier service.
User can reload digits shown on the graphs by pressing on the *Reload digits* button in the navbar.

User can logout from the application by pressing on the *Logout* button in the top right corner of the page.