import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationClient } from '../clients/authentication.client';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private tokenKey = 'token';

  constructor(
    private authenticationClient: AuthenticationClient,
    private router: Router
  ) {}


  public login(username: string, password: string): void {
    this.authenticationClient.login(username, password).subscribe((response) => {
      localStorage.setItem(this.tokenKey, response.access_token);
      this.router.navigate(['/']);
    });
  }

  public register(email: string, password: string, passwordConfirm: string): void {
    this.authenticationClient
      .register(email, password, passwordConfirm)
      .subscribe((response) => {
        this.router.navigate(['/login']);
      });
  }

  public verifyMail(token: string): void {
    this.authenticationClient
      .verifyMail(token)
      .subscribe((response) => {
        this.router.navigate(['/login']);
      });
  }

  public resetPassword(token: string, password: string, passwordConfirm: string): void {
    this.authenticationClient
      .resetPassword(token, password, passwordConfirm)
      .subscribe((response) => {
        this.router.navigate(['/login']);
      });
  }


  public resetPasswordRequest(email: string): void {
    this.authenticationClient
      .resetPasswordRequest(email)
      .subscribe((response) => {
        this.router.navigate(['/login']);
      });
  }


  public logout() {
    localStorage.removeItem(this.tokenKey);
    this.router.navigate(['/login']);
  }

  public isLoggedIn(): boolean {
    let token = localStorage.getItem(this.tokenKey);
    return token != null && token.length > 0;
  }

  public getToken(): string | null {
    return this.isLoggedIn() ? localStorage.getItem(this.tokenKey) : null;
  }
}
