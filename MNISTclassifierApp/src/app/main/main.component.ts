import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { ClassifierClient } from '../clients/classifier.client';
import { DigitSamples } from '../models/digitSamples.model';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit{

  public labels: string[] = ["", "", "", ""];

  private layout = {width: 350, height: 250, margin:{b:2, l:0, r:0, t:10, pad:10}}

  public graph1 = {
    data : [
      {
        z: [[0]],
        type: 'heatmap'
      }
    ],
    layout: this.layout
  };

  public graph2 = {
    data : [
      {
        z: [[0]],
        type: 'heatmap'
      }
    ],
    layout: this.layout
  };

  public graph3 = {
    data : [
      {
        z: [[0]],
        type: 'heatmap'
      }
    ],
    layout: this.layout
  };

  public graph4 = {
    data : [
      {
        z: [[0]],
        type: 'heatmap'
      }
    ],
    layout: this.layout
  };


  public samples : DigitSamples = {"data_samples":[]}

  constructor(
    private authenticationService: AuthenticationService,
    private classifierClient: ClassifierClient,
  ) {}

  private populateGraph(): void {
    this.classifierClient.getSamples().subscribe((response) => {
      this.samples = structuredClone(response);

      // Populate the graphs
      this.graph1.data[0].z = response.data_samples[0].reverse()
      this.graph2.data[0].z = response.data_samples[1].reverse()
      this.graph3.data[0].z = response.data_samples[2].reverse()
      this.graph4.data[0].z = response.data_samples[3].reverse()
    });
  }

  ngOnInit(): void {
    this.populateGraph();
  }

  public reloadDigits(): void {
    this.populateGraph();
  }


  public onSubmit(graph_num: number) {
    this.labels = ["", "", "", ""];
    this.classifierClient.getLabel(this.samples.data_samples[graph_num]).subscribe((response)=>{
      this.labels[graph_num] = "Label " + response.label
    });
  }

  logout(): void {
    this.authenticationService.logout();
  }

}
