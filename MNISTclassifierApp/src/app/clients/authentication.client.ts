import { environment } from '../../environments/environment';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Token } from '../models/token.model';
import { GenericResponse } from '../models/genericResponse.model';


@Injectable({
  providedIn: 'root',
})
export class AuthenticationClient {
  constructor(private http: HttpClient) {}

  public login(email: string, password: string): Observable<Token> {
    return this.http.post<Token>(
      environment.apiUrl + '/api/auth/login',
      {
        email: email,
        password: password,
      }
    );
  }

  public register(email: string, password: string, passwordConfirm: string): Observable<GenericResponse> {
    return this.http.post<GenericResponse>(
      environment.apiUrl + '/api/auth/register',
      {
        email: email,
        password: password,
        passwordConfirm: passwordConfirm
      }
    );
  }

  public verifyMail(token: string): Observable<GenericResponse> {
    return this.http.get<GenericResponse>(
      environment.apiUrl + '/api/auth/verifyemail/' + token
    );
  }

  public resetPassword(token: string, password: string, passwordConfirm: string): Observable<GenericResponse> {
    return this.http.post<GenericResponse>(
      environment.apiUrl + '/api/auth/password/reset', {
        token: token,
        password: password,
        passwordConfirm: passwordConfirm
      }
    );
  }


  public resetPasswordRequest(email: string): Observable<GenericResponse> {
    return this.http.get<GenericResponse>(
      environment.apiUrl + '/api/auth/password/reset/' + email.trim().toLowerCase()
    );
  }

}