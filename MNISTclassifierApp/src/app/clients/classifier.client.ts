import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DigitSamples } from '../models/digitSamples.model';
import { Label } from '../models/label.model';


@Injectable({
  providedIn: 'root',
})
export class ClassifierClient {
  constructor(private http: HttpClient) {}

  getSamples(num_samples: number = 4): Observable<DigitSamples> {
    return this.http.get<DigitSamples>(environment.apiUrl + '/api/classifier/samples?num_samples=' + num_samples);
  }

  getLabel(data_sample: Array<Array<number>>){
    return this.http.post<Label>(environment.apiUrl + '/api/classifier/predict', {
        input: data_sample
    });
  }
}