import { Component, OnInit} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-reset-password-request',
  templateUrl: './reset-password-request.component.html',
  styleUrls: ['./reset-password-request.component.css']
})
export class ResetPasswordRequestComponent implements OnInit{
    public resetPasswordRequestForm!: FormGroup;
    public message: string = "";

    constructor(private authenticationService: AuthenticationService){}

    ngOnInit(): void {
      this.resetPasswordRequestForm = new FormGroup({
        email: new FormControl('', [Validators.required, Validators.email])
      });
    }

    public onSubmit() {
      let email = this.resetPasswordRequestForm.get('email')!.value
      this.authenticationService.resetPasswordRequest(
        email
      );
      this.message = "The password reset code will be sent to your email ...";
    }
}
