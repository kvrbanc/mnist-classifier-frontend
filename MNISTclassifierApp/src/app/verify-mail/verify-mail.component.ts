import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';


@Component({
  selector: 'app-verify-mail',
  templateUrl: './verify-mail.component.html',
  styleUrls: ['./verify-mail.component.css']
})
export class VerifyMailComponent implements OnInit{

  public verificationForm!: FormGroup;
  public message: string = "";

  constructor(private route: ActivatedRoute, private authenticationService: AuthenticationService){}

  ngOnInit(): void {
    this.verificationForm = new FormGroup({
      token: new FormControl('', Validators.required)
    });

    this.route.queryParams.subscribe((params)=> {
      if (params['token']){
        // this.token = params['token']
        this.verificationForm.setValue({'token': params['token']})
        console.log(this.verificationForm.get('token')!.value)
      }
    })
  }

  public onSubmit() {
    this.authenticationService.verifyMail(
      this.verificationForm.get('token')!.value
    );
    this.message = "Verifying your acoount. Please wait..."
  }
}
