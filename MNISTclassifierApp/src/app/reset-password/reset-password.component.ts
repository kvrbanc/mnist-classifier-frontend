import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit{
  public resetForm!: FormGroup;
  private token: string = "";
  public message: string = "";

  constructor(private route: ActivatedRoute, private authenticationService: AuthenticationService){}

  ngOnInit(): void {
    this.resetForm = new FormGroup({
      password: new FormControl('', Validators.required),
      passwordConfirm: new FormControl('', Validators.required),
    });

    this.route.queryParams.subscribe((params)=> {
      if (params['token']){
        this.token = params['token'];
        console.log(this.token);
      }
    })
  }

  public onSubmit() {
    this.authenticationService.resetPassword(
      this.token,
      this.resetForm.get('password')!.value,
      this.resetForm.get('passwordConfirm')!.value
    );
    this.message = "Resetting your password. Please wait ..."
  }

}
